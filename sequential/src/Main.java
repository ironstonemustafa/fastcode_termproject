import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

class ValueComparator implements Comparator<String> {

    Map<String, Integer> base;
    public ValueComparator(Map<String, Integer> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.    
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}

public class Main {

	private static String filePath = "authors.txt";

	static BufferedReader inputFile = null;

	public static void main(String[] args) {
		Calendar startTime = Calendar.getInstance();

		TreeMap<String, Integer> map = new TreeMap<String ,Integer>();
		String[] toplist = new String[10];
		int count = 0;

		try {
			inputFile = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			String line;
			while ((line = inputFile.readLine()) != null) {
				String[] authors = line.split("@@");
				if(authors.length > 1){
					for(int i = 0; i<authors.length; i++){
						for(int j = i+1; j< authors.length; j++){
							if(map.containsKey(authors[i] + "&" + authors[j])){
								map.put(authors[i] + "&" + authors[j], map.get(authors[i] + "&" + authors[j]) + 1);
							}
							else if(map.containsKey(authors[j] + "&" + authors[i])){
								map.put(authors[j] + "&" + authors[i], map.get(authors[j] + "&" + authors[i]) + 1);
							}
							else{
								map.put(authors[i] + "&" + authors[j], 1);
							}
						}
					}
				}
				count++;
				if(count % 1000000 == 0){
					System.out.println(count);
				}
			}
//			count = 0;
//			for (Map.Entry<String, Integer> entry : map.entrySet())
//			{
//				//System.out.println(entry.getKey() + "-" + entry.getValue());
////				if(count > 6481162){
////					System.out.println(entry.getKey() + "-" + entry.getValue());
////				}
////				else{
////					count++;
////				}
//			}
			System.out.println(map.size());
			Calendar mapping = Calendar.getInstance();
			long timeElapsed = mapping.getTimeInMillis() - startTime.getTimeInMillis();
			System.out.println("Total mapping time: " + String.valueOf((timeElapsed / (1000 * 60))) + " min " + String.valueOf((timeElapsed / 1000) % 60) + " sec");
			System.out.println("Sort Began..");
			Set<Entry<String, Integer>> set = map.entrySet();
	        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
	        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
	        {
	            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
	            {
	                return (o2.getValue()).compareTo( o1.getValue() );
	            }
	        } );
			Calendar sorting = Calendar.getInstance();
			timeElapsed = sorting.getTimeInMillis() - mapping.getTimeInMillis();
			System.out.println("Total sorting time: " + String.valueOf((timeElapsed / (1000 * 60))) + " min " + String.valueOf((timeElapsed / 1000) % 60) + " sec");
			timeElapsed = sorting.getTimeInMillis() - startTime.getTimeInMillis();
			System.out.println("Total elapsed time: " + String.valueOf((timeElapsed / (1000 * 60))) + " min " + String.valueOf((timeElapsed / 1000) % 60) + " sec");
			for(int i = 0; i < 10; i++){
				System.out.println(list.get(i).getKey() + " -- " + list.get(i).getValue());
			}


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
