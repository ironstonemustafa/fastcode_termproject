package mapred.coauthor;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CoauthorReducer extends Reducer<CoauthorPair, IntWritable, IntWritable, Text> {

	@Override
	protected void reduce(CoauthorPair key, Iterable<IntWritable> value,
			Context context)
			throws IOException, InterruptedException {
		int count = 0;
		for (IntWritable n : value)
			count++;
		
		context.write(new IntWritable(count), new Text(key.toString()));
	}
}
