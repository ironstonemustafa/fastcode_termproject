package mapred.coauthor;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
 
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
 
public class CoauthorPair implements WritableComparable<CoauthorPair> {
 
    private Text first;
    private Text second;
 
    public CoauthorPair(Text first, Text second) {
        set(first, second);
    }
 
    public CoauthorPair() {
        set(new Text(), new Text());
    }
 
    public CoauthorPair(String first, String second) {
        set(new Text(first), new Text(second));
    }
 
    public Text getFirst() {
        return first;
    }
 
    public Text getSecond() {
        return second;
    }
 
    public void set(Text first, Text second) {
        this.first = first;
        this.second = second;
    }
 
    @Override
    public void readFields(DataInput in) throws IOException {
        first.readFields(in);
        second.readFields(in);
    }
 
    @Override
    public void write(DataOutput out) throws IOException {
        first.write(out);
        second.write(out);
    }
 
    @Override
    public String toString() {
        return first + "&" + second;
    }

    @Override
    public int compareTo(CoauthorPair tp) {
        int val1 = first.compareTo(tp.second);
        int val2 = second.compareTo(tp.first);

        if(val1 == 0 && val2 == 0){
            return 0;
        }
 
        if (val1 != 0) {
            return val1;
        }
 
        return second.compareTo(tp.second);
    }
 
    @Override
    public int hashCode(){
        return first.hashCode() + second.hashCode();
    }
 
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof CoauthorPair)
        {
            CoauthorPair tp = (CoauthorPair) o;
            return first.equals(tp.first) && second.equals(tp.second);
        }
        return false;
    }
 
}