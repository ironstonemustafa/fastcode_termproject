package mapred.coauthor;

import java.io.IOException;
import mapred.job.Optimizedjob;
import mapred.util.SimpleParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class Driver {

	public static void main(String args[]) throws Exception {
		SimpleParser parser = new SimpleParser(args);

		String input = parser.get("input");
		String output = parser.get("output");

		String sortJobInput = output + "/part-r-00000";
		String sortJobOutput = output + "/sorted";

		getJobFeatureVector(input, output);
		getSortedOutput(sortJobInput, sortJobOutput);

	}

	private static void getJobFeatureVector(String input, String output)
			throws IOException, ClassNotFoundException, InterruptedException {
		Optimizedjob job = new Optimizedjob(new Configuration(), input, output,
				"Compute Coauthorship Count");

		job.setClasses(CoauthorMapper.class, CoauthorReducer.class, null);
		job.setMapOutputClasses(CoauthorPair.class, IntWritable.class);

		job.run();
	}	

	private static void getSortedOutput(String input, String output)
			throws IOException, ClassNotFoundException, InterruptedException {
		Optimizedjob job = new Optimizedjob(new Configuration(), input, output,
				"Sort Coauthorship Count");

		job.setClasses(SortMapper.class, SortReducer.class, null);
		job.setMapOutputClasses(IntWritable.class, Text.class);

		job.run();
	}	
}
