package mapred.coauthor;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import mapred.util.Tokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CoauthorMapper extends Mapper<LongWritable, Text, CoauthorPair, IntWritable> {

	private final static IntWritable one = new IntWritable(1);
	TreeMap<String, Boolean> map = new TreeMap<String ,Boolean>();

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] authors = line.split("@@");

		for(int i = 0; i<authors.length; i++){
			for(int j = i+1; j< authors.length; j++){
				if(map.containsKey(authors[i] + "&" + authors[j])){
					context.write(new CoauthorPair(authors[i], authors[j]), one);
				}
				else if(map.containsKey(authors[j] + "&" + authors[i])){
					context.write(new CoauthorPair(authors[j], authors[i]), one);
				}
				else{
					context.write(new CoauthorPair(authors[i], authors[j]), one);
				}
			}
		}


	}
}
